# MessageBoxer #

Made: 2015 (Uploaded to Gitlab in 2017, however), last edited 2018 for uploading to NuGet


A DLL that has some helpers to create `MessageBox`es.

Made this small library because I was duplicating the code across a few 
different for both work and personal projects.


NuGet Link: 

## Installation Instructions ##

`Install-Package MessageBoxer` 

or 

Download the release .dll here: https://gitlab.com/wilcoforr/MessageBoxer/raw/master/MessageBoxer/bin/Release/MessageBoxer.dll (4.5KB)


## Example Usage ##

Used like so:

```cs
//Add reference to MessageBoxer.dll
using MessageBoxer;


//Example

    public static void Main()
    {
        ShowMessage.Notification("This is some text inside displaying some info", "Title/caption of message");
    }

```

