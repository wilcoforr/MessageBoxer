﻿using System.Windows.Forms;

namespace MessageBoxer
{
    /// <summary>
    /// A helper static class with static methods to shortcut the MessageBox.Show process
    /// Used at work and also in a few personal projects (WinForms and WPF alike)
    /// I found I was repeating the MessageBox.Show in quite a few projects I was 
    /// working on so I made this small .dll and/or static class
    /// (to copy paste over) to help with the process.
    /// 
    /// Used like so:
    ///     //Add reference to MessageBoxer.dll
    ///     //then:
    ///     using namespace MessageBoxer;
    ///     ...
    ///     Show.Error("Cannot delete this record! Reason: xyz", "Deleting RecordID: 123 failed!");
    /// </summary>
    public static class ShowMessage
    {
        /// <summary>
        /// Show an Alert box
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        public static void Alert(string text, string caption = "Alert!")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        /// <summary>
        /// Show a Informational box
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        public static void Notification(string text, string caption = "Notification")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Show a Error box
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        public static void Error(string text, string caption = "Error!")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Show a Warning box
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        public static void Warning(string text, string caption = "Warning!")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Show a Stop box
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        public static void Stop(string text, string caption = "Stop!")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        /// <summary>
        /// Get a DialogResult of a user's input of Yes or No
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static DialogResult GetUserYesOrNo(string text, string caption = "User Input Required")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Get a DialogResult of a user's input of Yes, No, or Cancel
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static DialogResult GetUserYesOrNoOrCancel(string text, string caption = "User Input Required")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
        }

        /// <summary>
        /// Get a DialogResult of a user's OK or Cancel
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static DialogResult GetUserOkOrCancel(string text, string caption = "OK or Cancel")
        {
            return MessageBox.Show(text, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
        }



    }
}
